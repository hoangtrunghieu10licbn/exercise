import path from 'path';
import logger from '../../helpers/log/logger';
import { createQuote } from '../models/quoteModel';
import { findRatePrice } from '../models/rateDataModel';
import { validateGetQuoteRequest } from '../../helpers/jsonSchemaValidate';

const getQuote = async (req) => {
  const validate = await validateGetQuoteRequest(req.body);
  console.log('validate', validate);

  if (!validate) {
    const message = 'Bad request';
    logger.error(`${path.basename(__filename)}| ${message}`);
    return message;
  }

  const { data: { origin: { address: { country_code: originValue } } } } = req.body;
  const { data: { destination: { address: { country_code: destinationValue } } } } = req.body;

  const { data: { package: { grossWeight: { unit: unitValue } } } } = req.body;
  let { data: { package: { grossWeight: { amount: weightValue } } } } = req.body;
  weightValue = Number(weightValue);

  // if (!originValue || !destinationValue || !unitValue || !weightValue) {
  //   const message = 'Not enough data from request';
  //   logger.error(`${path.basename(__filename)}| ${message}`);
  //   return message;
  // }
  logger.info(`${path.basename(__filename)}| weight value: ${weightValue}`);
  if (Number.isNaN(weightValue)) {
    logger.info(`${path.basename(__filename)}| Number.isNan => ${Number.isNaN(weightValue)}`);
    const message = 'Weight Value is not a number';
    logger.error(`${path.basename(__filename)}| ${message}`);
    return message;
  }
  if (weightValue <= 0) {
    const message = 'Weight is less than or equal 0';
    logger.error(`${path.basename(__filename)}| ${message}`);
    return message;
  }
  logger.info(`${path.basename(__filename)}| Unit Value: ${unitValue}`);
  // logger.info(`${path.basename(__filename)}| Type of Unit Value: ${typeof unitValue}`);
  // logger.info(`${path.basename(__filename)}| Type of Unit Value: ${typeof 'kg'}`);
  // if (unitValue !== 'kg' && unitValue !== 'g') {
  //   const message = 'Unit is invalid';
  //   logger.error(`${path.basename(__filename)}| ${message}`);
  //   return message;
  // }
  if (unitValue === 'kg') {
    weightValue *= 1000;
  }

  // logger.info(`${path.basename(__filename)}| weight value: ${weightValue}`);

  try {
    const data = await findRatePrice(originValue, destinationValue, weightValue);

    if (data.length <= 0) {
      const message = 'No data when find rate';
      logger.warn(`${path.basename(__filename)}| ${message}`);
      return message;
    }
    const quote = {
      amount: data[0].price,
    };

    logger.info(`${path.basename(__filename)}| quote: ${JSON.stringify(quote)}`);
    const dataQuote = await createQuote(quote);
    logger.info(`${path.basename(__filename)}| ${JSON.stringify(dataQuote)}`);

    const responseJSON = { data: [dataQuote] };
    return responseJSON;
  } catch (error) {
    logger.error(`${path.basename(__filename)}| ${error}`);
    const message = 'Failed to get quote';
    return message;
  }
};

export default getQuote;
