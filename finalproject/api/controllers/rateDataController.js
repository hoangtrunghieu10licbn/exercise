import path from 'path';
import logger from '../../helpers/log/logger';
import { loadRateData } from '../models/rateDataModel';

const loadRateDatabase = async () => {
  try {
    const data = await loadRateData();
    logger.info(`${path.basename(__filename)}| ${data}`);
    const response = 'Load rate data successfully !';
    return response;
  } catch (error) {
    logger.error(`${path.basename(__filename)}| ${error}`);
    return new Error(error);
  }
};

export default loadRateDatabase;
