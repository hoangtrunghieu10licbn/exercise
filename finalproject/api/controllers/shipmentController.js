import path from 'path';
import logger from '../../helpers/log/logger';
import { getAmount } from '../models/quoteModel';
import { createShipmentRecord, getShipmentRecord, deleteShipmentRecord } from '../models/shipmentModel';

const createReferenceNumber = () => {
  const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
  const stringLength = 10;
  let randomString = '';
  for (let i = 0; i < stringLength; i += 1) {
    const rnum = Math.floor(Math.random() * chars.length);
    randomString += chars.substring(rnum, rnum + 1);
  }
  return randomString;
};

const createShipment = async (req) => {
  const { data: shipment } = req.body;
  logger.info(`${path.basename(__filename)}| ${JSON.stringify(shipment)}`);

  const referenceNumber = createReferenceNumber();
  shipment.ref = referenceNumber;

  const day = new Date().toISOString();
  shipment.created_at = day;
  logger.info(`${path.basename(__filename)}| ${JSON.stringify(shipment)}`);

  const { data: { quote: { id: quoteID } } } = req.body;
  logger.info(`${path.basename(__filename)}| Quote ID: ${quoteID}`);

  try {
    const dataAmount = await getAmount(quoteID);
    if (dataAmount === undefined) {
      logger.warn(`${path.basename(__filename)}| No data when get amount with ID: ${quoteID}`);
      // res.status(500).send('Faild to get amount');
    } else {
      logger.info(`${path.basename(__filename)}| amount: ${dataAmount}`);
      const cost = dataAmount.amount;
      logger.info(`${path.basename(__filename)}| amount: ${cost}`);

      shipment.cost = cost;
      logger.info(`${path.basename(__filename)}| Shipment: ${JSON.stringify(shipment)}`);

      const shipmentRecord = await createShipmentRecord(shipment);
      logger.info(`${path.basename(__filename)}| shipment record: ${shipmentRecord}`);

      // const responseJSON = { data: [{ ref: referenceNumber, create_at: day, cost }] };
      // res.send(responseJSON);
    }
  } catch (error) {
    logger.error(`${path.basename(__filename)}| ${error}`);
    // res.status(500).send('Failed to create shipment');
  }
};

const getShipment = async (req, res) => {
  const { data: reference } = req.body;
  logger.info(`${path.basename(__filename)}| ${JSON.stringify(reference)}`);

  try {
    const shipments = await getShipmentRecord(reference.ref);
    if (shipments[0] === undefined) {
      const responseJSON = {
        data: {
          ref: '',
        },
      };
      res.status(404).send(responseJSON);
    } else {
      const responseJSON = {
        data: shipments[0],
      };
      res.status(404).send(responseJSON);
    }
  } catch (error) {
    logger.error(`${path.basename(__filename)}| Failed to get shipment`);
    const responseJSON = {
      data: {
        ref: '',
      },
    };
    res.status(404).send(responseJSON);
  }
};

const deleteShipment = async (req, res) => {
  const { data: { ref: referenceNumber } } = req.body;
  logger.info(`${path.basename(__filename)}| Reference Number: ${JSON.stringify(referenceNumber)}`);

  try {
    const deletedShipment = await deleteShipmentRecord(referenceNumber);
    if (deletedShipment === undefined) {
      logger.info(`${path.basename(__filename)}| Failed to delete that shipment`);
      const jsonResponse = {
        data: [
          {
            status: 'NOK',
            message: 'Failed to delete that shipment',
          },
        ],
      };
      res.send(jsonResponse);
    } else {
      const jsonResponse = {
        data: [
          {
            status: 'OK',
            message: 'shipment has been deleted',
          },
        ],
      };
      res.send(jsonResponse);
    }
  } catch (error) {
    logger.error(`${path.basename(__filename)}| ${error}`);
    const jsonResponse = {
      data: [
        {
          status: 'NOK',
          message: 'Failed to delete that shipment',
        },
      ],
    };
    res.status(500).send(jsonResponse);
  }
};

export { createShipment, getShipment, deleteShipment };

