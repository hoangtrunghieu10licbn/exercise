import mongoose from 'mongoose';
import path from 'path';
import logger from '../../helpers/log/logger';

const { Schema } = mongoose;

mongoose.Promise = Promise;

const quoteSchema = new Schema({
  amount: Number,
});

const Quote = mongoose.model('Quote', quoteSchema);

const createQuote = quote => Quote
  .create(quote)
  .then((result) => {
    logger.info(`${path.basename(__filename)}| ${result}`);
    return result;
  })
  .catch((error) => {
    logger.error(`${path.basename(__filename)}| ${error}`);
    return new Error(error);
  });

const getAmount = id => Quote
  .findById(id, 'amount')
  .exec()
  .then((result) => {
    logger.info(`${path.basename(__filename)}| ID ${id}`);
    return result;
  })
  .catch((error) => {
    logger.error(`${path.basename(__filename)}| ${error}`);
    return new Error(error);
  });

export { createQuote, getAmount };
