import mongoose from 'mongoose';
import path from 'path';
import logger from '../../helpers/log/logger';
import rateData from '../models/rateDatabase';

const { Schema } = mongoose;

mongoose.Promise = Promise;

const rateSchema = new Schema({
  weight: Number,
  price: Number,
  fromCountry: String,
  toCountry: String,
});

const RateData = mongoose.model('Rate', rateSchema);

const findRatePrice =
  (originValue, destinationValue, weightValue) => RateData
    .find({ fromCountry: originValue, toCountry: destinationValue })
    .sort({ weight: 1 })
    .where('weight').gte(weightValue)
    .limit(1)
    .select('price -_id')
    .exec()
    .then((result) => {
      logger.info(`${path.basename(__filename)}| ${result}`);
      return result;
    })
    .catch((error) => {
      logger.error(`${path.basename(__filename)}| ${error}`);
      return new Error(error);
    });

const loadRateData = () => RateData
  .create(rateData)
  .then((result) => {
    logger.info(`${path.basename(__filename)}| ${result}`);
    return result;
  })
  .catch((error) => {
    logger.error(`${path.basename(__filename)}| ${error}`);
    return new Error(error);
  });

export { loadRateData, findRatePrice };

