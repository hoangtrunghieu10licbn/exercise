import mongoose from 'mongoose';
import path from 'path';
import logger from '../../helpers/log/logger';


const { Schema } = mongoose;

mongoose.Promise = Promise;

const shipmentSchema = new Schema({
  quote: {
    id: String,
  },
  origin: {
    contact: {
      name: String,
      email: String,
      phone: String,
    },
    address: {
      country_code: String,
      locality: String,
      postal_code: String,
      address_line1: String,
      oraganisation: Boolean,
    },
  },
  destination: {
    contact: {
      name: String,
      email: String,
      phone: String,
    },
    address: {
      country_code: String,
      locality: String,
      postal_code: String,
      address_line1: String,
      oraganisation: Boolean,
    },
  },
  package: {
    dimensions: {
      height: Number,
      width: Number,
      length: Number,
      unit: ['cm'],
    },
    grossWeight: {
      amount: Number,
      unit: ['kg'],
    },
  },
  ref: String,
  created_at: Date,
  cost: Number,
});

const Shipment = mongoose.model('Shipment', shipmentSchema);

const createShipmentRecord = shipment => Shipment
  .create(shipment)
  .then((result) => {
    logger.info(`${path.basename(__filename)}| ${result}`);
    return result;
  })
  .catch((error) => {
    logger.error(`${path.basename(__filename)}| ${error}`);
    return new Error(error);
  });

const getShipmentRecord = referenceNumber => Shipment
  .find({ ref: referenceNumber }, '-cost -id -created_at -__v')
  .exec()
  .then((result) => {
    logger.info(`${path.basename(__filename)}| shipment founded: ${result}`);
    return result;
  })
  .catch((error) => {
    logger.error(`${path.basename(__filename)}| ${error}`);
  });


const deleteShipmentRecord = referenceNumber => Shipment
  .remove({ ref: referenceNumber })
  .exec()
  .then((result) => {
    logger.info(`${path.basename(__filename)}| ${result}`);
    return result;
  })
  .catch((error) => {
    logger.error(`${path.basename(__filename)}| ${error}`);
  });

export { createShipmentRecord, getShipmentRecord, deleteShipmentRecord };
