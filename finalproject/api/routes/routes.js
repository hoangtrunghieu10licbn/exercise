import express from 'express';
import path from 'path';
import getQuote from '../controllers/quoteController';
import loadRateDatabase from '../controllers/rateDataController';
import { createShipment, getShipment, deleteShipment } from '../controllers/shipmentController';
import logger from '../../helpers/log/logger';

const router = express.Router();

router.post('/api/loadRateData', async (req, res) => {
  try {
    const data = await loadRateDatabase();
    logger.info(`${path.basename(__filename)}| ${data}`);
    res.send(data);
  } catch (error) {
    logger.error(`${path.basename(__filename)}| ${error}`);
    res.status(500).send('Failed to load rate data');
  }
});

router.post('/client/getquote', async (req, res) => {
  try {
    const data = await getQuote(req);
    logger.info(`${path.basename(__filename)}| ${data}`);

    switch (data) {
      case 'Bad request':
        res.status(400).send('Bad request');
        break;
      // case 'Not enough data from request':
      //   res.status(500).send('Not enough data from request');
      //   break;
      case 'Weight Value is not a number':
        res.status(500).send('Weight Value is not a number');
        break;
      case 'Weight is less than or equal 0':
        res.status(500).send('Weight is less than or equal 0');
        break;
      // case 'Unit is invalid':
      //   res.status(500).send('Unit is invalid');
      //   break;
      case 'No data when find rate':
        res.status(500).send('No data when find rate');
        break;
      case 'Failed to get quote':
        res.status(500).send('Faild to get quote');
        break;
      default:
        res.send(data);
    }
  } catch (error) {
    logger.error(`${path.basename(__filename)}| ${error}`);
    res.status(500).send('Failed to get quote');
  }
});

router.post('/client/createshipment', async (req, res) => {
  try {
    const data = await createShipment(req);
  } catch (error) {
    logger.error(`${path.basename(__filename)}| ${error}`);
    res.status(500).send('Fail to create shipment');
  }
});

router.post('/client/getshipment', getShipment);

router.delete('/client/deleteshipment', deleteShipment);

export default router;
