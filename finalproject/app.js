import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import mongoose from 'mongoose';
import logger from './helpers/log/logger';


import router from './api/routes/routes';

import config from './config/index';

const app = express();
const port = process.env.PORT || 3000;
// const urlDB = 'mongodb://localhost:27017/shipping';

app.use('/assets', express.static(`${__dirname}/public`));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(morgan('dev'));
app.set('view engine', 'ejs');

mongoose.connect(config.getDbConnectionString(), (err) => {
  if (err) {
    logger.error(`${path.basename(__filename)}| Connect database fail!`);
  } else {
    logger.info(`${path.basename(__filename)}| Connect database successfully !`);
  }
});

app.use('/', router);

app.listen(port, () => {
  logger.info(`${path.basename(__filename)}| App is listening on port: ${port}`);
});
