import configValues from './config';

const getDbConnectionString = () => `mongodb://${configValues.username}${configValues.password}:27017/shipping`;

export default { getDbConnectionString };
