import Ajv from 'ajv';
import path from 'path';
import logger from './log/logger';

const ajv = Ajv({ allErrors: true });

const quoteRequestSchema = {
  title: 'quote',
  description: 'Get Quote request json',
  type: 'object',
  properties: {
    data: {
      type: 'object',
      properties: {
        origin: {
          type: 'object',
          properties: {
            contact: {
              type: 'object',
              properties: {
                name: { type: 'string', maxLength: 128 },
                email: { type: 'string', format: 'email' },
                phone: { type: 'string', minLength: 1 },
              },
              required: ['name', 'email', 'phone'],
            },
            address: {
              type: 'object',
              properties: {
                country_code: { type: 'string', minLength: 2, maxLength: 3 },
                locality: { type: 'string', minLength: 1, maxLength: 128 },
                postal_code: { type: 'string', minLength: 1, maxLenth: 128 },
                address_line1: { type: 'string', minLength: 1, maxLength: 128 },
              },
              required: ['country_code', 'locality', 'postal_code', 'address_line1'],
            },
          },
        },
        destination: {
          type: 'object',
          properties: {
            contact: {
              type: 'object',
              properties: {
                name: { type: 'string', maxLength: 128 },
                email: { type: 'string', format: 'email' },
                phone: { type: 'string', minLength: 1 },
              },
              required: ['name', 'email', 'phone'],
            },
            address: {
              type: 'object',
              properties: {
                country_code: { type: 'string', minLength: 2, maxLength: 3 },
                locality: { type: 'string', minLength: 1, maxLength: 128 },
                postal_code: { type: 'string', minLength: 1, maxLenth: 128 },
                address_line1: { type: 'string', minLength: 1, maxLength: 128 },
              },
              required: ['country_code', 'locality', 'postal_code', 'address_line1'],
            },
          },
        },
        package: {
          type: 'object',
          properties: {
            dimensions: {
              type: 'object',
              properties: {
                height: { type: 'number', minimum: 0 },
                width: { type: 'number', minimum: 0 },
                length: { type: 'number', minimum: 0 },
                unit: { type: 'string' },
              },
            },
            grossWeight: {
              type: 'object',
              properties: {
                amount: { type: 'number', minimum: 0 },
                unit: { type: 'string', enum: ['kg', 'g'] },
              },
              required: ['amount', 'unit'],
            },
          },
        },
      },
    },
  },
};

const shipmentRequestSchema = {
  title: 'shipment',
  description: 'create shipment request json',
  type: 'object',
  properties: {
    data: {
      type: 'object',
      properties: {
        quote: {
          type: 'object',
          properties: {
            id: { type: 'string', minLength: 1 },
          },
          required: ['id'],
        },
        origin: {
          type: 'object',
          properties: {
            contact: {
              type: 'object',
              properties: {
                name: { type: 'string', maxLength: 128 },
                email: { type: 'string', format: 'email' },
                phone: { type: 'string', minLength: 1 },
              },
              required: ['name', 'email', 'phone'],
            },
            address: {
              type: 'object',
              properties: {
                country_code: { type: 'string', minLength: 2, maxLength: 3 },
                locality: { type: 'string', minLength: 1, maxLength: 128 },
                postal_code: { type: 'string', minLength: 1, maxLenth: 128 },
                address_line1: { type: 'string', minLength: 1, maxLength: 128 },
              },
              required: ['country_code', 'locality', 'postal_code', 'address_line1'],
            },
          },
        },
        destination: {
          type: 'object',
          properties: {
            contact: {
              type: 'object',
              properties: {
                name: { type: 'string', maxLength: 128 },
                email: { type: 'string', format: 'email' },
                phone: { type: 'string', minLength: 1 },
              },
              required: ['name', 'email', 'phone'],
            },
            address: {
              type: 'object',
              properties: {
                country_code: { type: 'string', minLength: 2, maxLength: 3 },
                locality: { type: 'string', minLength: 1, maxLength: 128 },
                postal_code: { type: 'string', minLength: 1, maxLenth: 128 },
                address_line1: { type: 'string', minLength: 1, maxLength: 128 },
              },
              required: ['country_code', 'locality', 'postal_code', 'address_line1'],
            },
          },
        },
        package: {
          type: 'object',
          properties: {
            dimensions: {
              type: 'object',
              properties: {
                height: { type: 'number', minimum: 0 },
                width: { type: 'number', minimum: 0 },
                length: { type: 'number', minimum: 0 },
                unit: { type: 'string' },
              },
            },
            grossWeight: {
              type: 'object',
              properties: {
                amount: { type: 'number', minimum: 0, exclusiveMinimum: true },
                unit: { type: 'string', enum: ['kg', 'g'] },
              },
              required: ['amount', 'unit'],
            },
          },
        },
      },
    },
  },
};

const validateGetQuoteRequest = (request) => {
  const validate = ajv.compile(quoteRequestSchema);
  const valid = validate(request);

  if (!valid) {
    logger.error(`${path.basename(__filename)}| ${JSON.stringify(validate.errors)}`);
    return false;
  }
  return true;
};

const validateCreateShipmentRequest = (request) => {
  const validate = ajv.compile(shipmentRequestSchema);
  const valid = validate(request);

  if (!valid) {
    logger.error(`${path.basename(__filename)}| ${JSON.stringify(validate.errors)}`);
    return false;
  }
  return true;
};

export { validateGetQuoteRequest, validateCreateShipmentRequest };

