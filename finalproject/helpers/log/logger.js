import winston from 'winston';
import path from 'path';
import moment from 'moment';

const logger = new (winston.Logger)({
  transports: [
    // info console log
    new (winston.transports.Console)({
      level: 'info',
      name: 'info-console',
      colorize: true,
      timestamp: () => moment().format('YYYY-MM-DD HH-mm-ss'),
      // formatter: options =>
      //   `[${options.timestamp()}]: ${options.level.toUpperCase()}| ${options.message || ''}`,
    }),
    // info log file
    new (winston.transports.File)({
      level: 'info',
      name: 'info-file',
      filename: path.resolve(__dirname, 'development-info.log'),
      timestamp: () => moment().format('YYYY-MM-DD HH-mm-ss'),
      // formatter: options =>
      //   `[${options.timestamp()}]: ${options.level.toUpperCase()}| ${options.message || ''}`,
      json: false,
    }),
    // errors console log
    new (winston.transports.Console)({
      level: 'error',
      name: 'error-console',
      colorize: true,
      timestamp: () => moment().format('YYYY-MM-DD HH-mm-ss'),
      // formatter: options =>
      //   `[${options.timestamp()}]: ${options.level.toUpperCase()}| ${options.message || ''}`,
    }),
    // errors log file
    new (winston.transports.File)({
      level: 'error',
      name: 'error-file',
      filename: path.resolve(__dirname, 'development-errors.log'),
      timestamp: () => moment().format('YYYY-MM-DD HH-mm-ss'),
      // formatter: options =>
      //   `[${options.timestamp()}]: ${options.level.toUpperCase()}| ${options.message || ''}`,
      json: false,
    }),
    // warn console log
    new (winston.transports.Console)({
      level: 'warn',
      name: 'warn-console',
      colorize: true,
      timestamp: () => moment().format('YYYY-MM-DD HH-mm-ss'),
      // formatter: options =>
      //   `[${options.timestamp()}]: ${options.level.toUpperCase()}| ${options.message || ''}`,
    }),
    // warn log file
    new (winston.transports.File)({
      level: 'warn',
      name: 'warn-file',
      filename: path.resolve(__dirname, 'development-warn.log'),
      timestamp: () => moment().format('YYYY-MM-DD HH-mm-ss'),
      // formatter: options =>
      //   `[${options.timestamp()}]: ${options.level.toUpperCase()}| ${options.message || ''}`,
      json: false,
    }),
  ],
});

export default logger;
