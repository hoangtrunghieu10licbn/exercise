let calculateTheSum = (array) => {
    let length = array.length;
    let sum = 0;
    for(let i = 0; i < length; i++){
        sum += array[i];
    }
    return sum;
}

var array = [1, 2, 3, 4, 5, 6];
console.log(calculateTheSum(array));