let sort = (array) => {
    let length = array.length;
    for (let i = (length - 1); i >= 0; i--) {
		for (let j = (length - i); j > 0; j--) {
			if (array[j] < array[j - 1]) {
				[array[j], array[j-1]] = [array[j-1], array[j]];
			}
		}
    }
    
    for(let i = 0; i < length; i++){
        process.stdout.write(array[i] + " ");
    }
}

let array = [-3, 8, 7, 6, 5, -4, 3, 2, 1];
sort(array);