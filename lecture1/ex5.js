let power_of_2 = (number) => {
    while(true){
        if(number < 1){
            return false;
        }
        if(number == 2 || number == 1){
            return true;
        } 
        if(number%2 === 0) {
            number = number/2;
        } else {
            return false;
        }
    }
}

console.log(power_of_2(16));
console.log(power_of_2(18));
console.log(power_of_2(256));
