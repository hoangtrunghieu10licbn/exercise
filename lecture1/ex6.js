let getPropertiesName = (object) => {
    let array = Object.getOwnPropertyNames(object);
    let length = array.length;
    for(let i = 0; i < length; i++){
        process.stdout.write(array[i]);
        if(i < length - 1) {
            process.stdout.write(", ");
        }
    }
}

var student = {
    name: "David Ravy",
    sclass: "VI",
    rollno: 12
};

getPropertiesName(student);