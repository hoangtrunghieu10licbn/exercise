let is_weekend =  (inputDate) => {
    let date = new Date(inputDate).getDay();

    return (date == 6 || date == 0) ? "weekend" : "not weekend"
     
    // if(date.getDay() == 6 || date.getDay() == 0){
    //     return "weekend";
    // } else {
    //     return "not weekend";
    // }
    
}

console.log(is_weekend('Nov 15, 2014'));
console.log(is_weekend('Nov 16, 2014'));
console.log(is_weekend('Nov 17, 2014'));