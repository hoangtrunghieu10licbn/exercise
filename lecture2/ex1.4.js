let inventory = [
    { type:     "machine",  value: 5000},
    { type:     "machine",  value:  650},
    { type:        "duck",  value:   10},
    { type:   "furniture",  value: 1200},
    { type:     "machine",  value:   77}
];

let totalMachineValue = (inventory) => {
    //the first way
    // return inventory.filter(machines => machines['type'] == "machine")
    // .reduce((sum, amount) => sum + amount.value, 0);
    //the second way
    let inventoryFiltered = inventory.filter(machines => machines['type'] == "machine");
    return inventoryFiltered.map(vl => vl.value).reduce((sum, amount) => sum + amount, 0);
}

console.log(totalMachineValue(inventory));

