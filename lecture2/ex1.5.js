function Car() {
    this.speed = 0;
    let that = this;
    setTimeout(function () {
        that.speed += 10;
    }, 100);
}

var c = new Car();
setTimeout(function () {
    console.log(c.speed);
}, 200);
