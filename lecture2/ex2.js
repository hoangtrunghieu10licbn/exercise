class Animal {
    constructor (name) {
        this.name = name;
        this.thirst = 100;
        this.belly = [];
    }

    drink() {
        this.thirst -= 10;
    }

    eat(food) {
        this.belly.push(food);
    }
}

class Dog extends Animal {
    constructor(name, breed) {
        super(name);
        this.breed = breed;
    }

    bark() {
        console.log("woof-woof");
    }
}

let dogg = new Dog("dogg", "gg");
dogg.bark();
