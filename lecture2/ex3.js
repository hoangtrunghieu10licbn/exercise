function getPrice (service) {
    var price = 50.0;

    return {
        service,
        price,

        discount(rate) {
            if (rate <= 0 || rate > 1) {
                throw new Error('Invalid discount rate');
            }
          
            price *= 1 - rate;
        },

        get price() {
            return price;
        }
    };
}

let priceDetail = getPrice("EXPRESS");
priceDetail.discount(0.2);

console.log(priceDetail.price);
