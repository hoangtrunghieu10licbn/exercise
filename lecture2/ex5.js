var person = {
    name: 'Joe',
    age: 21,
    married: true,
    hobbies: ['football', 'tennis', 'cooking'],
    contact: {
        phone: '0123456789',
        email: 'hieuht@fsoft.com.vn'
    },
};

var getNameAndAge = ({name, age}) => {
    return [name, age];
}

console.log(getNameAndAge(person));

let myName, myAge;
[myName, myAge] = getNameAndAge(person);

console.log(myName + myAge);

let getHobbies = ({hobbies}) => {
    return hobbies;
}

let hobby1, hobby2;
[hobby1, hobby2] = getHobbies(person);

// console.log(hobby1 + hobby2);

let getMarried = ({married}) => {
    return married;
}

console.log(getMarried(person));

let getPhoneAndEmail = ({contact}) => {
    let phone, email;
    [phone, email="you@mail.com"] = [contact.phone, contact.email];
    return [phone, email];
}

console.log(getPhoneAndEmail(person));