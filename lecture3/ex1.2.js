var array1 = [1, 2, 3];
var randomValue = 'something';
var array2 = [4, 5, 6];

let concat = (array1, randomValue, array2) => {
    let result = [];
    let lengthOfArray1 = array1.length;
    for(let i = 0; i < lengthOfArray1; i++) {
        result.push(array1[i]);
    }
    result.push(randomValue);
    let lengthOfArray2 = array2.length;
    for(let i = 0; i < lengthOfArray2; i++) {
        result.push(array2[i]);
    }

    return result; 
}

console.log(concat(array1, randomValue, array2));
console.log(array1);
console.log(array2);