let sum = (x, ...y) => {
    let sum = 0;
    sum += x;
    let lengthOfY = y.length;
    for(let i = 0; i <lengthOfY; i++) {
        sum += y[i];
    }

    return sum;
}

console.log(sum(5, 6, 7, 9, 10, 12));