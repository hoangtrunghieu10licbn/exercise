let sum = (x, ...y) => {
    let sum = 0;
    sum += x;
    for(let i = 0; i < 2; i++) {
        if(!isNaN(y[i]))
            sum += y[i];
    }

    return sum;
}

console.log(sum(5, 6, 0, 9));