let getCountdownIterator = {
    [Symbol.iterator]() {
        let cur = 9;
        return {
            next() {
                return cur > 0 ? { done: false, value: cur-- } : {done: true};
            },
        };
    },
};

console.log([...getCountdownIterator]);

