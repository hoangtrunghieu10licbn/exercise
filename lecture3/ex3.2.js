let fibonacci = (n) => {
    if(n == 0) return 0;
    if(n == 1) return 1;
    return fibonacci(n-1) + fibonacci(n-2);
}

let fib = {
    [Symbol.iterator]() {
        let cur = 0;
        return {
            next() {
                return { done: false, value: fibonacci(cur++) };
            },
        };
    },
};

for (let n of fib) {
    if(n > 100) {
        break;
    }
    console.log(n);
}