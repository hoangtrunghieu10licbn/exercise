let getCountdownIterator = function *() {
    let i = 10;
    while( i > 1 ) {
        yield --i;
    }
}
 
console.log( [ ...getCountdownIterator() ] );
