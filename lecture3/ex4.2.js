function *fibonacci() {
    let a = 0, b = 1;
    yield a;
    yield b;
    while( true ) {
        [a, b] = [b, a+b];
        yield b;
    }
}

for (let n of fibonacci()) {
    if(n > 1000) {
        break;
    }
    console.log(n);
}