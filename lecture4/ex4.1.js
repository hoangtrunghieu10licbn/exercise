let array = [NaN, 0, 16, false, -21, '', undefined, 45, null];

let filter = (array) => {
    let length = array.length;
    for(let i = 0; i < length; i++) {
        let temp = array[i];
        if(Number.isNaN(temp) 
            || temp == 0
            || !Boolean(temp)
            || temp == undefined 
            || temp === '') {
                array.splice(i, 1);
                length--;
                i--;
            }
    }

    return array;
}

console.log(filter(array));