let array = [2, 5, 9, 6];
let value = 9;

let removeArrayElement = (array, value) => {
    let length = array.length;
    for(let i = 0; i < length; i++) {
        if(array[i] == value) {
            array.splice(i, 1);
            length--;
            i--;
        }
    }

    return array;
}

console.log(removeArrayElement(array, value));