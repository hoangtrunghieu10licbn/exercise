let array = [
    {
        id: 1,
        scores: [7, 8, 3, 4]
    },
    {
        id: 2,
        scores: [5, 10, 9, 6]
    },
    {
        id: 3,
        scores: [9, 7, 4, 8]
    }
];

let totalAllScores = (array) => {
    return array.reduce((accumulator, currentValue) => accumulator + currentValue.scores.reduce((sum, amount) => sum + amount, 0), 0);
}

console.log(totalAllScores(array));