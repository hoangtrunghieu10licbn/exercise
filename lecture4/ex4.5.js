let array = [5, 10, 15, 20, 25, 30];

let increaseArrayElement = (array) => {
    let length = array.length;
    for(let i = 0; i < length; i++) {
        array[i] += 10;
    }

    return array;
}

console.log(increaseArrayElement(array));