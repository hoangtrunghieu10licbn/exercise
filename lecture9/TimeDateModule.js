let date = new Date();

exports.getDate = () => {
    return Date();
}

exports.getTime = () => {
    let date = new Date();
    return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
}