console.log('Hello world');

console.log(`There are ${60*60*24*365} seconds in a year`);
console.log(`There are ${60*60*24*365*100} seconds in a year`);

let timeTillChristmas = () => {
    let today = new Date();
    let christmas =  new Date(2018, 11, 25);

    let difference = Math.round((christmas.getTime() - today.getTime())/1000);
    let secondsInADay = 60*60*24;
    let day = Math.floor(difference/secondsInADay);
    let hours = Math.floor((difference%secondsInADay)/(60*60));
    let minutes = Math.floor(((difference%secondsInADay)%(60*60))/60);
    let seconds = Math.floor(((difference%secondsInADay)%(60*60))%60);
    console.log(`There are only ${day} days ${hours} hours ${minutes} minutes ${seconds} seconds until Christmas!`);

    let t = setTimeout(timeTillChristmas, 1000);
}

timeTillChristmas();