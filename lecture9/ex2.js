var http = require("http");
const port = 3000;
const ip = "127.0.0.1"
let str = "Hello world";

http.createServer((request, response) => {
    // console.log(request);

    // Send the HTTP header
    // HTTP Status: 200 : OK
    // Content Type: text/plain
    // response.writeHead(200, {'Content-Type': 'text/plan'});

    // Send the response body as "Hello World"
    // response.end(str);

    //route thủ công bằng if else
    if(request.url === "/") {
        response.writeHead(200, {'Content-Type': 'text/plan'});
        response.end("Hello HieuHT11 from DTL.Ship");
    } else if (request.url === "/nodejsfresher"){
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.end(`<h1>${request.url}</h1>`);
    } else if (request.url === "/nodetest") {
        response.writeHead(200, { 'Content-Type': 'text/html'});
        response.end(`<h1>Node Test</h1>`);
    } else if (request.url === "/about") {
        response.writeHead(200, { 'Content-Type': 'text/html'});
        response.end(`<h1 style="text-align: center; color:blue">NodeJS</h1>
                    <h2 style="text-align: center; color:red">Training NodeJS Temando</h2>
                    <h3 style="text-align: center; color:violin">Exercises</h3>
                    <h4 style="text-align: center; color:green">Lecture 9</h4>
                    <h5 style="text-align: center; color:brown">NodeJS Summary</h5>`);
    } else {
        response.writeHead(404);
        response.end(`404 Not Found`);
    }


}).listen(port);

//Console will print the message
// console.log(`Server running at http://${ip}:${port}`);


