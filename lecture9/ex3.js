let http = require("http");
let fs = require("fs");
const port = 3000;

http.createServer((request, response) => {
    if(request.url === "/") {
        response.writeHead(200, {'Content-Type': 'text/plan'});
        response.end(__dirname);
    } else if (request.url === "/index.html") {
        fs.createReadStream(__dirname + "/index.html").pipe(response);
    } else {
        response.writeHead(404);
        response.end('Not Fucking Found bro');
    }
    
}).listen(port);

fs.readFile(__dirname + "/index.html", "utf8", function(err, data) {
    if (err) {
        console.error('There was an error reading the file! - 404', err);
        return;
    }
    console.log(data);
})

