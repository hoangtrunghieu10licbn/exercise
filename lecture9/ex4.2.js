let http = require("http");
let fs = require("fs");
let router = require("./routerModule");

const port = 3000;

http.createServer((req, res) => {
    router(req, res, __dirname);
}).listen(port);


