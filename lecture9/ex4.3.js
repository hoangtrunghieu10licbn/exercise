let http = require("http");
let router = require("./routeWithController");

const port = 3000;

http.createServer((req,res) => {
    router(req, res);
}).listen(port);