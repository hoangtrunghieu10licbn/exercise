let http = require("http");
let rp = require("request-promise");
const port = 3000;

http.createServer((req, res) => {
    //http://api.openweathermap.org/data/2.5/weather?q=London,uk
    rp('http://samples.openweathermap.org/data/2.5/forecast?id=524901&appid=b1b15e88fa797225412429c1c50c122a1')
        .then(function (response) {
            res.writeHead(200, {'Content-Type': 'text/plan'});
            res.end(response);
        })
        .catch(function (err) {
            // Crawling failed...
            res.writeHead(404);
            res.end("Not found GG");
        });
}).listen(port);