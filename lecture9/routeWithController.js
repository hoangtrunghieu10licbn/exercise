let controller = require("./controllerModule");

module.exports = function (req, res) {
    if(req.url === "/") {
        controller.home(res, req);
    } else if (req.url === "/hello") {
       controller.hello(res, req);
    } else if(req.url === "/goodbye") {
        controller.goodbye(res, req);
    } else {
        controller.notFound(res, req);
    }
}