let fs = require("fs");

/* viết module route phải truyền res, req và dirname làm tham số, nếu ko require fs ở file module route thì cũng
truyền fs vào làm tham số */
module.exports = function (req, res, __dirname) {
    if(req.url === "/") {
        fs.createReadStream(__dirname + "/index.html").pipe(res);
        console.log("Hello World");
    } else if (req.url === "/hello") {
        fs.createReadStream(__dirname + "/hello.html").pipe(res);
    } else if(req.url === "/goodbye") {
        fs.createReadStream(__dirname + "/goodbye.html").pipe(res);
    } else {
        res.writeHead(404);
        res.end("Not Fucking found bro");
    }
}